<#
    .SYNOPSIS
        動き補償を利用して動画のフレームレートを変換する
    .NOTES
        - `ffmpeg -hide_banner -h filter=minterpolate`
        - https://ffmpeg.org/ffmpeg-filters.html#minterpolate
#>
Param(
    [Parameter(Mandatory=$True)][String]$VideoPath,
    [Parameter(Mandatory=$True)][String]$OutputFrameRate,
    [ValidateSet("dup", "blend", "mci")][String]$MotionInterpolationMode = "mci",
    [ValidateSet("obmc", "aobmc")][String]$MotionCompensationMode = "obmc",
    [ValidateSet("bidir", "bilat")][String]$MotionEstimationMode = "bilat",
    [ValidateSet(
        "esa", "tss", "tdls", "ntss", "fss", "ds", "hexbs", "epzs", "umh"
    )][String]$MotionEstimation = "epzs",
    [ValidateRange(4, 16)][Int32]$MacroBlockSize = 16,
    [ValidateRange(4, [Int32]::MaxValue)][int32]$SearchParameter = 32,
    [ValidateSet(0, 1)][Int32]$VariableSizeBlockMotionCompensation = 0,
    [ValidateSet("none", "fdiff")][String]$SceneChangeDetectionMethod = "fdiff",
    [ValidateRange(0.0, 100.0)][double]$SceneChangeDetectionThreshold = 5.0,
    [String]$VideoCodec = "utvideo",
    [String]$AudioCodec = "pcm_s16le",
    [String]$PixelFormat = "yuv444p",
    [String]$OutputFileName = "$(Get-Date -Format yyyy-MM-dd_HH-mm-ss)"
)

$VideoFilter = @(
    "minterpolate=",
    "fps=${OutputFrameRate}:",
    "mi_mode=${MotionInterpolationMode}:",
    "mc_mode=${MotionCompensationMode}:",
    "me_mode=${MotionEstimationMode}:",
    "me=${MotionEstimation}:",
    "mb_size=${MacroBlockSize}:",
    "search_param=${SearchParameter}:",
    "vsbmc=${VariableSizeBlockMotionCompensation}:",
    "scd=${SceneChangeDetectionMethod}:",
    "scd_threshold=${SceneChangeDetectionThreshold}"
) -join ""

ffmpeg -hide_banner `
       -i $VideoPath `
       -vcodec $VideoCodec `
       -pix_fmt $PixelFormat `
       -vf $VideoFilter `
       -acodec $AudioCodec `
       "${OutputFileName}.avi"
if ($LASTEXITCODE -ne 0) { throw "FFMpeg でエラー" }

exit 0
