Param(
    [Parameter(Mandatory=$True, ValueFromPipeline=$True)][String]$Path,
    [String]$StartTime = "0:00",
    [String]$Duration,
    [ValidateRange(-1, 69)][Int32]$Quality = 1,
    [String]$FromExtension,
    [ValidateSet(
        "bmp", "dpx", "exr", "jls", "jpeg", "jpg", "jxl", "ljpg", "pam", "pbm", "pcx", "pfm",
        "pgm", "pgmyuv", "phm", "png", "ppm", "sgi", "tga", "tif", "tiff", "jp2", "j2c", "j2k",
        "xwd", "sun", "ras", "rs", "im1", "im8", "im24", "sunras", "vbn", "xbm", "xface", "pix",
        "y", "avif", "qoi", "hdr", "wbmp"
    )][String]$ToExtension = "png"
)

if (!(Test-Path -Path $Path)) { throw "指定されたパスが不正: $Path" }
$Item = Get-Item -Path $Path
$ParentPath = Split-Path $Item

if (!($Item.PSIsContainer)) {
    # ファイルが指定された場合は動画とみなしてフレーム画像を生成する
    $DistDir = New-Item -ItemType "Directory" `
                        -Path $ParentPath `
                        -Name "${ToExtension}_converted" `
                        -Force

    $Arguments = @(
        "-hide_banner",
        "-i $Path"
        "-f image2",
        "-qmin 1",
        "-q $Quality",
        "-ss $StartTime"
    )
    if ($Duration -ne "") {
        $Arguments += "-t $Duration"
    }
    $Arguments += "-start_number 0"
    $Arguments += Join-Path $DistDir "%04d.$ToExtension"
    $Arguments = $Arguments -join " "

    Start-Process "ffmpeg" -ArgumentList $Arguments -NoNewWindow
    if ($LASTEXITCODE -ne 0) { throw "ffmpeg でエラー" }

    return $DistDir
}

# フォルダが指定された場合は、指定された拡張子の画像ファイルを JPEG に変換する
if ($FromExtension -eq "") { throw "変換対象の拡張子が指定されていない" }
if (!($FromExtension -like ".*")) {
    $FromExtension = "." + $FromExtension
}

$ImageList = Get-ChildItem -Path $Path `
           | Where-Object { $_.Extension -eq $FromExtension } `
           | Sort-Object -Property "Name"
if ($ImageList.Count -eq 0) { throw "指定された拡張子の画像が見つからなかった" }

$DistDir = New-Item -ItemType "Directory" -Path $Path -Name "${ToExtension}_converted" -Force
$ImageList | Foreach-Object {
    ffmpeg -hide_banner `
           -i $_.FullName `
           -f "image2" `
           -qmin 1 `
           -q $Quality `
           (Join-Path $DistDir "$($_.BaseName).$ToExtension")
}
if ($LASTEXITCODE -ne 0) { throw "FFMpeg でエラー" }

return $DistDir
