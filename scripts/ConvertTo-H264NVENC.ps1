Param(
    [Parameter(Mandatory=$True, ValueFromPipeline=$True)][String]$VideoPath,
    [Int32]$BFrame = 2,
    [String]$Profile = "high",
    [String]$PixelFormat = "yuv420p",
    [Int32]$GoPSize = 15,
    [String]$AudioBitrate = "320k"
)
if (!(Test-Path -Path $VideoPath)) { throw "動画のパスが不正" }
$Video = Get-Item -Path $VideoPath
if ($Viode.PSIsContainer) { throw "動画パスでは無くフォルダパスが指定された" }

$ParentPath = Split-Path -Path $Video.FullName
$OutputPath = Join-Path $ParentPath "$($Video.BaseName)_h264-nvenc.mp4"

ffmpeg -hide_banner `
       -i $VideoPath `
       -c:v "h264_nvenc" `
       -pix_fmt $PixelFormat `
       -g $GoPSize `
       -bf $BFrame `
       -forced-idr $True `
       -b:v 0 `
       -rc "vbr" `
       -tune "hq" `
       -2pass $True `
       -profile:v $Profile `
       -c:a "libmp3lame" `
       -b:a $AudioBitrate `
       $OutputPath
if ($LASTEXITCODE -ne 0) { throw "ffmpeg でエラー" }

return $OutputPath
