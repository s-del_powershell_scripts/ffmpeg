Param(
    [parameter(mandatory=$true)][string]$TargetVideoPath,
    [parameter(mandatory=$true)][string]$SourceVideoPath,
    [Switch]$OutFile
)
if (!(Test-Path -Path $TargetVideoPath)) { throw "指定された比較対象動画ファイルが存在しない" }
if ($TargetVideoPath.PSIsContainer) { throw "指定された比較対象動画のパスがフォルダ" }
if (!(Test-Path -Path $SourceVideoPath)) { throw "指定されたソース動画ファイルが存在しない" }
if ($SourceVideoPath.PSIsContainer) { throw "指定されたソース動画のパスがフォルダ" }

ffmpeg -hide_banner `
       -i $TargetVideoPath `
       -i $SourceVideoPath `
       -filter_complex ($OutFile ? "scale2ref,ssim=ssim.txt" : "scale2ref,ssim") `
       -an `
       -f null `
       NUL
if ($LASTEXITCODE -ne 0) { throw "FFMpeg でエラー" }

exit 0
