Param(
    [Parameter(Mandatory=$True)][String]$VideoPath,
    [ValidateSet(
        "default", "compact", "csv", "flat", "ini", "json", "xml"
    )][String]$OutputFormat = "default"
)
if(!(Test-Path -Path $VideoPath)) { throw "動画のパス指定が不正" }

ffprobe -hide_banner `
        -show_format `
        -show_streams `
        -show_chapters `
        -output_format $OutputFormat `
        -pretty `
        -loglevel "error" `
        -i $VideoPath
if ($LASTEXITCODE -ne 0) { throw "FFMpeg でエラー" }

exit 0
