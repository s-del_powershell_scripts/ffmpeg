<#
    .LINK
        - https://x265.readthedocs.io/en/stable/index.html
        - https://trac.ffmpeg.org/wiki/Encode/H.265
#>
Param(
    [Parameter(Mandatory=$True, ValueFromPipeline=$True)][String]$SrcVideoPath,
    [Parameter(ParameterSetName="VBR")][Switch]$VBR,
    [Parameter(ParameterSetName="VBR")][String]$TargetBitRate = "2000k",
    [ValidateRange(0, 51)]$CRF = 28,
    [ValidateRange(-1, [float]::MaxValue)]$GoPSize = 250,
    [ValidateSet(
        "ultrafast", "superfast", "veryfast", "faster", "fast",
        "medium",
        "slow", "slower", "veryslow", "placebo"
    )][String]$Preset = "medium",
    [ValidateSet(8, 10)]$OutputBitDepth = 8,
    [ValidateSet(
        "yuv420p", "yuv420p10le", "yuv444p", "yuv444p10le"
    )]$PixelFormat = "yuv420p",

    [String]$AudioCodec = "libopus",
    [String]$AudioBitrate = "96k"
)

if (!(Test-Path -Path $SrcVideoPath)) { throw "指定された動画ファイルが存在しない" }
if ($SrcVideoPath.PSIsContainer) { throw "指定されたパスがフォルダ" }
$SrcFile = Get-Item -Path $SrcVideoPath
$CrntDirPath = Convert-Path "."

if ($VBR) {
    $OutputPath = Join-Path $CrntDirPath "$($SrcFile.BaseName)_hevc_2pass-vbr.mp4"

    ffmpeg -hide_banner `
           -i $SrcVideoPath `
           -c:v libx265 `
           -b:v $TargetBitRate `
           -preset $Preset `
           -g $GoPSize `
           -pix_fmt $PixelFormat `
           -x265-params "pass=1" `
           -an `
           -f null `
           NUL
    if ($LASTEXITCODE -ne 0) { throw "1st pass でエラー" }

    ffmpeg -hide_banner `
           -strict -2 `
           -i $SrcVideoPath `
           -c:v libx265 `
           -b:v $TargetBitRate `
           -preset $Preset `
           -g $GoPSize `
           -pix_fmt $PixelFormat `
           -x265-params "pass=2" `
           -c:a $AudioCodec `
           -vbr on `
           -b:a $AudioBitrate `
           $OutputPath
    if ($LASTEXITCODE -ne 0) { throw "2nd pass でエラー" }

    return $OutputPath
}

$OutputPath = Join-Path $CrntDirPath "$($SrcFile.BaseName)_hevc_crf.mp4"

ffmpeg -hide_banner `
       -strict -2 `
       -i $SrcVideoPath `
       -c:v libx265 `
       -crf $CRF `
       -preset $Preset `
       -g $GoPSize `
       -pix_fmt $PixelFormat `
       -c:a $AudioCodec `
       -vbr on `
       -b:a $AudioBitrate `
       $OutputPath
if ($LASTEXITCODE -ne 0) { throw "ffmpeg でエラー" }

return $OutputPath
