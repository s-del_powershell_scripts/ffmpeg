<#
    .SYNOPSIS
        動画を SVT-AV1 でエンコードする。
    .DESCRIPTION
        指定されたパスの動画を SVT-AV1 でエンコードする。
        デフォルトの音声コーデックは Opus。
    .PARAMETER SrcVideoPath
        必須 (文字列): エンコードしたい動画のパス。
    .PARAMETER VBR
        指定した場合、ビットレート指定の VBR エンコードになる。
    .PARAMETER TargetBitRate
        VBR エンコードでの目標ビットレートを指定する。
    .PARAMETER CRF
        CRF 値を指定する (1 - 63)
    .PARAMETER EncoderMode
        プリセット値を指定する。
        1 ~ 3 は時間が掛かるが高効率で圧縮される。
        4 ~ 6 は時間と効率のバランスが良い。
        7 以上はストリーミング用など。
    .PARAMETER Tune
        チューニング指標を決定する。
        0 は視覚指標、1 は PSNR、2 は SSIM。
    .PARAMETER GoPSize
        GoP サイズを指定する。(毎秒フレーム数 * 秒数)
    .PARAMETER EncoderBitDepth
        8bit か 10bit の色深度を整数で指定する。
    .PARAMETER PixelFormat
        ピクセルフォーマットを指定する。
    .PARAMETER AudioCodec
        音声コーデックを指定する。
    .PARAMETER AudioBitrate
        音声ビットレートを指定する
    .EXAMPLE
        ConvertTo-Av1 -SrcVideoPath "path\to\video" `
                      -VBR `
                      -TargetBitRate 7500 `
                      -EncoderMode 6 `
                      -Keyint 300
    .LINK
        - https://gitlab.com/AOMediaCodec/SVT-AV1/-/blob/master/Docs/Parameters.md
        - https://gitlab.com/AOMediaCodec/SVT-AV1/-/blob/master/Docs/CommonQuestions.md#what-presets-do
        - https://nico-lab.net/ffmpeg_with_svtav1/
    .COMPONENT
        - ffmpeg
            - libsvtav1
            - libopus
#>
Param(
    [Parameter(Mandatory=$True, ValueFromPipeline=$True)][String]$SrcVideoPath,
    [Parameter(ParameterSetName="VBR")][Switch]$VBR,
    [Parameter(ParameterSetName="VBR")][String]$TargetBitRate = "2000k",
    [ValidateRange(1, 63)]$CRF = 30,
    [ValidateRange(-1, 13)]$EncoderMode = 6,
    [ValidateRange(0, 2)]$Tune = 0,
    [ValidateRange(-2, [Int32]::MaxValue)]$GoPSize = 90,
    [ValidateSet(8, 10)]$EncoderBitDepth = 8,
    [ValidateSet("yuv420p", "yuv420p10le")]$PixelFormat = (
        $EncoderBitDepth -eq 10 ? "yuv420p10le" : "yuv420p"
    ),

    [String]$AudioCodec = "libopus",
    [String]$AudioBitrate = "128k"
)

if (!(Test-Path -Path $SrcVideoPath)) { throw "指定された動画ファイルが存在しない" }
if ($SrcVideoPath.PSIsContainer) { throw "指定されたパスがフォルダ" }
$SrcFile = Get-Item -Path $SrcVideoPath
$CrntDirPath = Convert-Path "."

$SvtAv1Params = "preset=${EncoderMode}:" `
              + "tune=${Tune}:" `
              + "input-depth=${EncoderBitDepth}:"

if ($VBR) {
    $SvtAv1Params += "rc=1:"
    $SvtAv1Params += "tbr=${TargetBitRate}"
    $OutputPath = Join-Path $CrntDirPath "$($SrcFile.BaseName)_av1_2pass-vbr.mp4"

    ffmpeg -hide_banner `
           -i $SrcVideoPath `
           -c:v libsvtav1 `
           -g $GoPSize `
           -pix_fmt $PixelFormat `
           -svtav1-params $SvtAv1Params `
           -pass 1 `
           -an `
           -f null `
           NUL
    if ($LASTEXITCODE -ne 0) { throw "1st pass でエラー" }

    ffmpeg -hide_banner `
           -strict -2 `
           -i $SrcVideoPath `
           -c:v libsvtav1 `
           -g $GoPSize `
           -pix_fmt $PixelFormat `
           -svtav1-params $SvtAv1Params `
           -pass 2 `
           -c:a $AudioCodec `
           -vbr on `
           -b:a $AudioBitrate `
           $OutputPath
    if ($LASTEXITCODE -ne 0) { throw "2nd pass でエラー" }

    return $OutputPath
}

$SvtAv1Params += "rc=0:"
$SvtAv1Params += "crf=${CRF}"
$OutputPath = Join-Path $CrntDirPath "$($SrcFile.BaseName)_av1_crf.mp4"

ffmpeg -hide_banner `
       -strict -2 `
       -i $SrcVideoPath `
       -c:v libsvtav1 `
       -g $GoPSize `
       -pix_fmt $PixelFormat `
       -svtav1-params $SvtAv1Params `
       -c:a $AudioCodec `
       -vbr on `
       -b:a $AudioBitrate `
       $OutputPath
if ($LASTEXITCODE -ne 0) { throw "ffmpeg でエラー" }

return $OutputPath
