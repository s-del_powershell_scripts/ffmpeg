Param(
    [Parameter(Mandatory=$True)][String]$TargetVideoPath,
    [Parameter(Mandatory=$True)][String]$SourceVideoPath
)

$FilterComplex = "libvmaf=model=version=vmaf_v0.6.1\\:" `
               + "name=vmaf\\:" `
               + "log_path=vmaf.json\\:" `
               + "log_fmt=json"

ffmpeg -hide_banner `
       -i $TargetVideoPath `
       -i $SourceVideoPath `
       -filter_complex $FilterComplex `
       -an `
       -f null `
       NUL
if ($LASTEXITCODE -ne 0) { throw "ffmpeg でエラー" }

exit 0
