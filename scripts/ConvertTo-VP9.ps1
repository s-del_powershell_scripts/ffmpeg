<#
    .SYNOPSIS
        動画を VP9 でエンコードする。
    .DESCRIPTION
        指定されたパスの動画を VP9 でエンコードする。
        デフォルトの音声コーデックは Opus。
    .PARAMETER SrcVideoPath
        必須 (文字列): エンコードしたい動画のパス。
    .PARAMETER VBR
        指定した場合、ビットレート指定の VBR エンコードになる。
        指定しなかった場合は CRF でのエンコードになる。
    .PARAMETER TargetBitRate
        VBR での目標ビットレートを指定する。
    .PARAMETER CRF
        指定した場合、CRF でのエンコードになる。
    .PARAMETER Keyint
        GoP サイズを指定する。(毎秒フレーム数 * 秒数)
    .EXAMPLE
        ConvertTo-Vp9 -SrcVideoPath "path\to\video" `
                      -VBR `
                      -TargetBitRate 7500 `
                      -Keyint 300
    .LINK
        - https://trac.ffmpeg.org/wiki/Encode/VP9
        - https://nico-lab.net/setting_libvpx-vp9_with_ffmpeg/
    .COMPONENT
        - ffmpeg
            - libvpx-vp9
            - libopus
#>
Param(
    [parameter(mandatory=$true)][string]$SrcVideoPath,
    [parameter(ParameterSetName="VBR")][switch]$VBR,
    [parameter(ParameterSetName="VBR")][string]$TargetBitRate = "2000k",
    [ValidateRange(1, 63)]$CRF = 30,
    [ValidateSet("best", "good", "realtime")]$DeadLine = "good",
    [int]$Keyint = 15,
    [string]$PixelFormat = "yuv420p",

    [string]$AudioCodec = "libopus",
    [string]$AudioBitrate = "96k"
)
if (!(Test-Path -Path $SrcVideoPath)) { throw "指定された動画ファイルが存在しない" }
if ($SrcVideoPath.PSIsContainer) { throw "指定されたパスがフォルダ" }

$SrcFile = Get-Item $SrcVideoPath
$CrntDirPath = Convert-Path "."

if ($VBR) {
    ffmpeg -hide_banner `
           -i $SrcVideoPath `
           -c:v "libvpx-vp9" `
           -pix_fmt $PixelFormat `
           -b:v $TargetBitRate `
           -g $Keyint `
           -deadline $DeadLine `
           -pass 1 `
           -an `
           -f null `
           NUL
    if ($LASTEXITCODE -ne 0) { throw "1st pass でエラー" }

    $OutputPath = Join-Path $CrntDirPath "$($SrcFile.BaseName)_vp9_2pass-vbr.webm"
    ffmpeg -hide_banner `
           -i $SrcVideoPath `
           -c:v "libvpx-vp9" `
           -pix_fmt $PixelFormat `
           -b:v $TargetBitRate `
           -g $Keyint `
           -deadline $DeadLine `
           -pass 2 `
           -c:a $AudioCodec `
           -b:a $AudioBitrate `
           -vbr "on" `
           $OutputPath
    if ($LASTEXITCODE -ne 0) { throw "2nd pass でエラー" }

    exit 0
}

$OutputPath = Join-Path $CrntDirPath "$($SrcFile.BaseName)_vp9_crf.webm"
ffmpeg -hide_banner `
       -i $SrcVideoPath `
       -c:v "libvpx-vp9" `
       -pix_fmt $PixelFormat `
       -crf $CRF `
       -b:v 0 `
       -g $Keyint `
       -deadline $DeadLine `
       -c:a $AudioCodec `
       -b:a $AudioBitrate `
       -vbr "on" `
       $OutputPath
if ($LASTEXITCODE -ne 0) { throw "ffmpeg でエラー" }

exit 0
