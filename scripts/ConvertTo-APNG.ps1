<#
    .SYNOPSIS
        動画からアニメーション APNG 画像を作成する
#>
Param(
    [Parameter(Mandatory=$True)][String]$VideoPath,
    [Int32]$Loop = 0,
    [String]$FrameRate = "24000/1001",
    [String]$OutputName = "$(Get-Date -Format yyyy-MM-dd_HH-mm-ss)"
)

ffmpeg -hide_banner `
       -i $VideoPath `
       -plays $Loop `
       -r $FrameRate `
       -an `
       "${OutputName}.apng"
if ($LASTEXITCODE -ne 0) { throw "ffmpeg でエラー" }

exit 0
